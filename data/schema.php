<?php
$db = new PDO('sqlite:' . realpath(__DIR__) . '/data.db');
$db->exec('CREATE TABLE message (id INTEGER PRIMARY KEY, [time] TIMESTAMP NOT NULL, [text] TEXT NOT NULL);');
