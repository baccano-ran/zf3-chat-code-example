$(function() {

    function redrawTable(messages) {
        let tableBody = $('#tableBody');

        tableBody.html('');

        messages.forEach((item) => {
            let date = (new Date(item.time * 1000)).toISOString().slice(0, 19).replace("T", " ");

            tableBody.append(`<tr><td>${date}</td><td>${item.text}</td></tr>`);
        });
    }

    function getMessages() {
        $.get('/api/get')
            .then(redrawTable)
            .catch(() => {
                alert('Data retrieval error');
            });
    }

    $('form').submit((e) => {
        e.preventDefault();

        let messageText = $('#textInput').val().trim();

        if(messageText === '') {
            return;
        }

        $.ajax({
            url: '/api/put',
            data: {
                text: messageText
            }
        })
            .then(redrawTable)
            .catch(() => {
                alert('Data retrieval error');
            });
    });

    getMessages();
});