<?php

namespace Application\Controller;

use Application\Model\Message;
use Application\Model\MessageRepository;
use Application\Module;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class ApiController extends AbstractActionController
{
    const MESSAGES_COUNT = 10;

    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * ApiController constructor.
     * @param MessageRepository $messageRepository.
     */
    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * @return JsonModel
     */
    public function indexAction()
    {
        return new JsonModel([
            'api_version' => Module::VERSION
        ]);
    }

    /**
     * @return JsonModel
     */
    public function getAction()
    {
        return new JsonModel(
            $this->messageRepository->getAll()
        );
    }

    /**
     * @return JsonModel
     */
    public function putAction()
    {
        $text = $this->params()->fromQuery('text');

        if(empty($text)) {
            return new JsonModel([
                'error' => 'text parameter is required'
            ]);
        }

        $message = new Message();
        $message->setTime(time());
        $message->setText($text);

        $this->messageRepository->insert($message);

        $this->messageRepository->removeOld(self::MESSAGES_COUNT);

        return new JsonModel(
            $this->messageRepository->getAll()
        );
    }

}
