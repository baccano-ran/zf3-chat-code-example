<?php

namespace Application;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    const VERSION = '0.1-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\MessageRepository::class => function($container) {
                    $tableGateway = $container->get('MessageRepositoryGateway');
                    return new Model\MessageRepository($tableGateway);
                },
                'MessageRepositoryGateway' => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Message());
                    return new TableGateway('message', $dbAdapter, null, $resultSetPrototype);
                },
            ]
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\ApiController::class => function($container) {
                    return new Controller\ApiController(
                        $container->get(Model\MessageRepository::class)
                    );
                },
            ]
        ];
    }
}
