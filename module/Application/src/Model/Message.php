<?php

namespace Application\Model;

class Message
{
    const ID_FIELD = 'id';
    const TIME_FIELD = 'time';
    const TEXT_FIELD = 'text';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $time;

    /**
     * @var string
     */
    private $text;

    /**
     * @param array $data
     */
    public function exchangeArray(array $data)
    {
        $this->id = empty($data['id']) ? null : $data['id'];
        $this->time = empty($data['time']) ? null : $data['time'];
        $this->text = empty($data['text']) ? null : htmlspecialchars($data['text']);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param int $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = htmlspecialchars($text);
    }
}
