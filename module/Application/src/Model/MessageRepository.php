<?php

namespace  Application\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGatewayInterface;

class MessageRepository
{
    /**
     * @var TableGatewayInterface
     */
    private $tableGateway;

    /**
     * MessageRepository constructor.
     * @param TableGatewayInterface $tableGateway
     */
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->tableGateway->select(function(Select $select) {
            $select->order(Message::TIME_FIELD . ' DESC');
        })->toArray();
    }

    /**
     * @param Message $message
     */
    public function insert(Message $message)
    {
        $this->tableGateway->insert([
            Message::TIME_FIELD => $message->getTime(),
            Message::TEXT_FIELD => $message->getText(),
        ]);
    }

    /**
     * @param int $count
     */
    public function removeOld(int $count)
    {
        $messages = $this->tableGateway->select()->toArray();
        $messagesForRemove = array_slice($messages, 0, count($messages) - $count);
        $messagesIdsForeRemove = array_column($messagesForRemove, 'id');

        $this->tableGateway->delete([
            'id' => $messagesIdsForeRemove
        ]);
    }
}
